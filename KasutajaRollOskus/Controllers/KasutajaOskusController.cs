﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KasutajaRollOskus;

namespace KasutajaRollOskus.Controllers
{
    public class KasutajaOskusController : Controller
    {
        private UusRaamatEntities db = new UusRaamatEntities();

        // GET: KasutajaOskus
        public ActionResult Index()
        {
            var kasutajaOskus = db.KasutajaOskus.Include(k => k.Kasutaja).Include(k => k.Oskus);
            return View(kasutajaOskus.ToList());
        }

        // GET: KasutajaOskus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KasutajaOskus kasutajaOskus = db.KasutajaOskus.Find(id);
            if (kasutajaOskus == null)
            {
                return HttpNotFound();
            }
            return View(kasutajaOskus);
        }

        // GET: KasutajaOskus/Create
        public ActionResult Create()
        {
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Nimi");
            ViewBag.OskusId = new SelectList(db.Oskus, "Id", "Nimetus");
            return View();
        }

        // POST: KasutajaOskus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,KasutajaId,OskusId,Tase")] KasutajaOskus kasutajaOskus)
        {
            if (ModelState.IsValid)
            {
                db.KasutajaOskus.Add(kasutajaOskus);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Nimi", kasutajaOskus.KasutajaId);
            ViewBag.OskusId = new SelectList(db.Oskus, "Id", "Nimetus", kasutajaOskus.OskusId);
            return View(kasutajaOskus);
        }

        // GET: KasutajaOskus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KasutajaOskus kasutajaOskus = db.KasutajaOskus.Find(id);
            if (kasutajaOskus == null)
            {
                return HttpNotFound();
            }
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Nimi", kasutajaOskus.KasutajaId);
            ViewBag.OskusId = new SelectList(db.Oskus, "Id", "Nimetus", kasutajaOskus.OskusId);
            return View(kasutajaOskus);
        }

        // POST: KasutajaOskus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,KasutajaId,OskusId,Tase")] KasutajaOskus kasutajaOskus)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kasutajaOskus).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Nimi", kasutajaOskus.KasutajaId);
            ViewBag.OskusId = new SelectList(db.Oskus, "Id", "Nimetus", kasutajaOskus.OskusId);
            return View(kasutajaOskus);
        }

        // GET: KasutajaOskus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KasutajaOskus kasutajaOskus = db.KasutajaOskus.Find(id);
            if (kasutajaOskus == null)
            {
                return HttpNotFound();
            }
            return View(kasutajaOskus);
        }

        // POST: KasutajaOskus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KasutajaOskus kasutajaOskus = db.KasutajaOskus.Find(id);
            db.KasutajaOskus.Remove(kasutajaOskus);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
