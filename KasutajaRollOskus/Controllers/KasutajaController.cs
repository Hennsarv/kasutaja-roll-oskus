﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KasutajaRollOskus;
using KasutajaRollOskus.Models; // selle lisasin, et saaks mõnusasti data-klasse kasutada

// lisan namespace ilma Controllers osata ja sinna Kasutaja partial klassi
namespace KasutajaRollOskus
{
    partial class Kasutaja
    {
        // selle rea võiks iga dataklassi laienduse ja kontrolleri algusse lisada
        static UusRaamatEntities db = new UusRaamatEntities();

        // teen ühe funktsiooni, mis annab meili järgi kasutaja
        public static Kasutaja OldByEmail(string email)
        {
            return db.Kasutaja.Where(x => x.Email == email).Take(1).SingleOrDefault();
        }

        public static Kasutaja ByEmail(String email)
        {
            Kasutaja kasutaja = OldByEmail(email);
            if (kasutaja == null)
            {
                db.Kasutaja.Add(kasutaja = new Kasutaja { Email = email, Nimi = "poleveelnime" });
                db.SaveChanges();
                db.KasutajaRoll.Add(new KasutajaRoll { KasutajaId = kasutaja.Id, RollId = 0 });
                db.SaveChanges();

            }
            return kasutaja;
        }

        // lisan selle välja, et saaks oskuseid lisada
        public string UusOskus { get; set; }

        
        

    }
}

namespace KasutajaRollOskus.Controllers
{
    public class KasutajaController : Controller
    {
        private UusRaamatEntities db = new UusRaamatEntities();

        // GET: Kasutajas
        public ActionResult Index()
        {
            // lisame meetodi algusse kasutaja kontrolli
            if (!Request.IsAuthenticated) return RedirectToAction("Index", "Home");
            Kasutaja k = Kasutaja.ByEmail(User.Identity.Name);
            if (k == null) return RedirectToAction("Index", "Home");

            // siia hiljem ka kontroll, kas on admin

            return View(db.Kasutaja.ToList());
        }

        // GET: Kasutajas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kasutaja kasutaja = db.Kasutaja.Find(id);
            if (kasutaja == null)
            {
                return HttpNotFound();
            }
            return View(kasutaja);
        }

        // GET: Kasutajas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Kasutajas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nimi,Email")] Kasutaja kasutaja)
        {
            if (ModelState.IsValid)
            {
                db.Kasutaja.Add(kasutaja);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kasutaja);
        }

        // GET: Kasutajas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kasutaja kasutaja = db.Kasutaja.Find(id);
            if (kasutaja == null)
            {
                return HttpNotFound();
            }
            // siin lisan ViewBagi rolliloetelu, et View ei peaks seda 
            // andmebaasist küsima
            ViewBag.Rollid = db.Roll.ToList();
            kasutaja.UusOskus = "";
            return View(kasutaja);
        }

        // POST: Kasutajas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nimi,Email,UusOskus")] Kasutaja kasutaja)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kasutaja).State = EntityState.Modified;
                db.SaveChanges();
                // lisame kontrolli, kas lisati uus oskus
                string uusOskus = (kasutaja.UusOskus + "").Trim().ToLower();
                if (uusOskus == "") return RedirectToAction("Index");
                Oskus o = db.Oskus.Where(x => x.Nimetus.ToLower() == uusOskus).Take(1).SingleOrDefault();
                if (o == null)
                {
                    o = new Oskus { Nimetus = kasutaja.UusOskus };
                    db.SaveChanges();
                }
                kasutaja.KasutajaOskus.Add(new KasutajaOskus { Oskus = o });
                db.SaveChanges();
                return RedirectToAction($"Edit/{kasutaja.Id}");
            }
            return View(kasutaja);
        }

        public ActionResult LisaRoll(int? id)
        {
            if (!id.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            int kasutajaId = id.Value / 1000;
            int rollId = id.Value % 1000;
            db.KasutajaRoll.Add(new KasutajaRoll { KasutajaId = kasutajaId, RollId = rollId });
            db.SaveChanges();
            return RedirectToAction($"Edit/{kasutajaId}");
        }

        public ActionResult EemaldaRoll(int? id)
        {
            if (!id.HasValue) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            int kasutajaId = id.Value / 1000;
            int rollId = id.Value % 1000;

            KasutajaRoll kr = db.KasutajaRoll.Where(x => x.KasutajaId == kasutajaId && x.RollId == rollId).SingleOrDefault();
            if (kr != null)
            {
                db.KasutajaRoll.Remove(kr);
                db.SaveChanges();
            }

            return RedirectToAction($"Edit/{kasutajaId}");

        }


        // GET: Kasutajas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kasutaja kasutaja = db.Kasutaja.Find(id);
            if (kasutaja == null)
            {
                return HttpNotFound();
            }
            return View(kasutaja);
        }

        // POST: Kasutajas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kasutaja kasutaja = db.Kasutaja.Find(id);
            db.Kasutaja.Remove(kasutaja);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
