﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KasutajaRollOskus.Startup))]
namespace KasutajaRollOskus
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
