﻿Teen nii nagu mitu korda varem MVC rakenduse koos autentimisega
Lisan Entiti frameworki andmebaasiga ja buildin
KOrrigeerin DefaultConnectioni connection stringi WebConfigis

Mul on tabelid
	Kasutaja (Id, Nimi, Email)
	Roll (Id, Nimetus)
	KasutajaRoll (Id, KasutajaId, RollId)
	Oskus (Id, Nimetus)
	KasutajaOskus (Id, KasutajaId, OskusId, Tase)

Esimeseks lisan kasutaja kontrolleri
	Proovin kasutajate tabelit selle kaudu (TEst)
	Proovin sisse-välja logimist - kõik toimib

Nüüd proovin teha ühenduse sisselogimisesüsteemi ja oma kasutajate vahel
Selleks (valin selle variandi, kus ma täiendan oma Kasutaja klassi)
	Lisan static funktsiooni, mis leiab meiliaadressi järgi Kasutaja
	Vaata Kasutaja controlleri algusse
	Proovin seda funktsiooni _LoginPartial views - vaata sinna
	(selles Views kuvatakse tervitus sisselogitud kasutajale)
	KOntrollin, et toimib - sisselogitud kasutajale kuvatakse Tere Nimi
	võõrale Hello email

Lisan nüüd Kasutaja valiku üles menüüsse, nii et see ilmub ainult 
sisselogitud meie kasutajale vaata _Layout viewd

Lisan nüüd ka rollide kontrolleri ja viewd ning _Layouti viite ka sellele

Lisan nüüd Kasutaja edit lehele tema rollid võimalusega neid lisada
	Vaata Kasutaja view Edit
	Esialgu loetelu kõigist rollidest
	Seejärel loetelu kasutaja rollidest
	Siis filtreerin välja need, kes juba on kasutajal

	Lõpuks lisan Kasutaja kontrollerisse meetodid LisaRoll ja KustutaRoll
	parameetriks 1000*KasutajaId + RollId (kuna ma ei viitsi targemat välja mõelda)
	Ja lisan eelnevatele lingid nende meetodite väljakutsumiseks



