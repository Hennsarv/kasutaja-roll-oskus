//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KasutajaRollOskus
{
    using System;
    using System.Collections.Generic;
    
    public partial class KasutajaRoll
    {
        public int id { get; set; }
        public int KasutajaId { get; set; }
        public int RollId { get; set; }
    
        public virtual Kasutaja Kasutaja { get; set; }
        public virtual Roll Roll { get; set; }
    }
}
